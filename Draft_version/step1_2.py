# -*- coding: utf-8 -*-
"""
Created on Fri May  1 22:22:21 2020

@author: admin
"""

import os
import pm4py
from tkinter import *
from tkinter.filedialog import askopenfilename
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.util import constants
from pm4py.objects.log.importer.xes import factory as xes_import_factory
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory


def import_csv_file(filepath):
    dataframe = csv_import_adapter.import_dataframe_from_path(os.path.join(filepath), sep=",")
    log_csv = conversion_factory.apply(dataframe)
    return log_csv

def import_xes_file(filepath):
    log_xes = xes_import_factory.apply(filepath)
    return log_xes

#generat petri net
def generate_petri_net(log):
    net, initial_marking, final_marking = inductive_miner.apply(log)
    parameters = {"format":"png"}
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking,variant="performance", parameters=parameters)
    pn_vis_factory.save(gviz, "petri_net.png")
    

def simulation():
    window = Tk()
    window.title('Simulating Event Log')
    window.geometry('1068x681+10+10')
    #open file only.xes and .csv
    filepath = askopenfilename( filetype =(("XES Files","*.xes"),("CSV Files","*.csv"),("All Files","*.*")) )
    
    #GUI_label
    init_data_label = Label(window, text="real event log")
    init_data_label.grid(row=0, column=0)
    petri_net_label = Label(window, text="generated petri net")
    petri_net_label.grid(row=0, column=12)
    log_label = Label(window, text="simulated event log")
    log_label.grid(row=12, column=0)
    
    #GUI_Text
    init_data_Text = Text(window, width=67, height=35)  #for original data
    init_data_Text.grid(row=1, column=0, rowspan=10, columnspan=10)
    petri_net_Text = Text(window, width=70, height=49)  #for petri net
    petri_net_Text.grid(row=1, column=12, rowspan=15, columnspan=10)
    log_data_Text = Text(window, width=66, height=9)  # for simulated petri net
    log_data_Text.grid(row=13, column=0, columnspan=10)
    
    
    #display the inputted event log
    file_name, file_extension = os.path.splitext(filepath)
    file_extension = file_extension.replace("'))", "")   
    if file_extension == ".xes":
        log = import_xes_file(filepath)
    else:
        log = import_csv_file(filepath)
    init_data_Text.insert(INSERT, log)
    
    #prcocess disvovery
    generate_petri_net(log)
    
    #display the generated petri net
    #petri_net = PhotoImage(file="petri_net.png") 
    #petri_net_Text.image_create(END, image=petri_net)

    
    
    window.mainloop()


simulation()
