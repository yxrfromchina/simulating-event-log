import numpy as np
import pandas as pd
import pm4py
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from pm4py.objects.log.importer.xes import factory as xes_import_factory
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.util import constants
from pm4py.statistics.traces.log import case_statistics
from pm4py.algo.filtering.log.variants import variants_filter
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as petrinet_vis_factory
from pm4py.evaluation import factory as evaluation_factory
from pm4py.algo.conformance.tokenreplay import factory as token_based_replay_factory
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.evaluation.replay_fitness import factory as replay_fitness_factory
from pm4py.algo.discovery.dfg import factory as dfg_factory
from pm4py.visualization.dfg import factory as dfg_vis_factory

class Root(Tk):
    def __init__(self):
        super(Root, self).__init__()
        self.title("Generating Simulated Event Log")
        self.minsize(640, 400)
 
        self.labelFrame = ttk.LabelFrame(self, text = "Import Event Log")
        self.labelFrame.grid(column = 0, row = 1, padx = 20, pady = 20)
 
        self.button()
 
 
 
    def button(self):
        self.button = ttk.Button(self.labelFrame, text = "Browse Event Log",command = self.fileDialog)
        self.button.grid(column = 1, row = 1)
 
 
    def fileDialog(self):

        label_title1 = "File path: "
        label_title2 = "Event Log info: "
        self.label = ttk.Label(self.labelFrame, text = label_title1, wraplength=500)
        self.label.grid(column = 1, row = 2)
        self.filename = filedialog.askopenfilename(initialdir =  "C:/Users/User/Dropbox/Simulation Project - Process Discovery Using Python/Assignment3 Sprint1 Report & Demo/Faizan", title = "Locate Event Log", filetype =
        (("XES Files","*.xes"),("CSV Files","*.csv"),("All Files","*.*")) )
        self.label = ttk.Label(self.labelFrame, text = "", wraplength=500)
        self.label.grid(column = 1, row = 3)
        self.label.configure(text = self.filename)
        
        # Import event log
        event_log = xes_import_factory.apply(self.filename)
        
        # Mine petri net with inductive miner algorithm
        net, initial_marking, final_marking = inductive_miner.apply(event_log)
        
        # Visualize petri net
        parameters = {"format":"jpeg"}
        gviz_petrinet = petrinet_vis_factory.apply(net, initial_marking, final_marking, parameters=parameters)
        petrinet_vis_factory.view(gviz_petrinet)
        pn_vis_factory.save(gviz_petrinet, "PetriNet.jpeg")
        
        # Calculate process model evaluation metrics
        # var_evaluation_3 = evaluation_factory.apply(event_log, net, initial_marking, final_marking)

        # print("Process model evaluation metrics as follows:")
        # print("Deviation point for first trace:",token_replay_result_3[0]['transitions_with_problems'])
        # print("Percentage of traces with perfect fit: {0:.2f}%".format(fitness_tokenbasedreplay_3['percFitTraces']*100))
        # print("Precision: {0:.4f}".format(var_evaluation_3['precision']))
        # print("Simplicity: {0:.4f}".format(var_evaluation_3['simplicity']))
        # print("Generalization: {0:.4f}".format(var_evaluation_3['generalization']))


        dfg = dfg_factory.apply(event_log)
        # Visualize with performance information
        parameters = {"format":"png"}
        gviz_dfg = dfg_vis_factory.apply(dfg, log=event_log, variant='performance', parameters=parameters)
        dfg_vis_factory.view(gviz_dfg)
        dfg_vis_factory.save(gviz_dfg, "Model_with_performance_information.png")

        self.label = ttk.Label(self.labelFrame, text = label_title2, wraplength=500)
        self.label.grid(column = 1, row = 4)
        self.label = ttk.Label(self.labelFrame, text = log_info, wraplength=500)
        self.label.grid(column = 1, row = 20)
             
 

root = Root()
root.mainloop()
