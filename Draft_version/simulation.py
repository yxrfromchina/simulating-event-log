import argparse
import os
from pathlib import Path
from datetime import datetime
import statistics
from pm4py.objects.log.importer.xes import factory as xes_import_factory
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.evaluation import factory as evaluation_factory
from pm4py.algo.conformance.tokenreplay import factory as token_based_replay_factory
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.evaluation.replay_fitness import factory as replay_fitness_factory
from pm4py.algo.discovery.dfg import factory as dfg_factory
from pm4py.visualization.dfg import factory as dfg_vis_factory
from pm4py.util.business_hours import BusinessHours
from pm4py.objects.log.util.xes import DEFAULT_TIMESTAMP_KEY
from pm4py.util.constants import PARAMETER_CONSTANT_TIMESTAMP_KEY
from pm4py.statistics.traces.log import case_statistics
from pm4py.algo.filtering.log.attributes import attributes_filter as log_attributes_filter
from pm4py.visualization.graphs import factory as graphs_factory
from pm4py.statistics.attributes.log import get as attributes_filter
from pm4py.objects.petri import utils


# This function reads the input file path from the command line
def read_input_file_path():
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path", type=Path)
    file = parser.parse_args()
    print("File received: ", file.file_path)
    if file.file_path.exists():
        print("File exists")
    else:
        print("File does not exist. Please input correct file")
        exit()
    return str(file.file_path)


# This function imports the event log from a .xes file
def import_xes(file_path):
    xes_log = xes_import_factory.apply(file_path)
    print("Import of xes successful,with {0} traces in total".format(len(xes_log)))
    return xes_log


# This function imports the event log from a .csv file
def import_csv(file_path):
    data_frame = csv_import_adapter.import_dataframe_from_path(
        os.path.join(file_path), sep=",")
    csv_log = conversion_factory.apply(data_frame)
    print("Import of csv successful,with {0} traces in total".format(len(csv_log)))
    return csv_log


# This function verifies that the extension of the event log file is .xes or .csv and imports the logs from those files
def verify_extension_and_import():
    file_path = read_input_file_path()
    file_name, file_extension = os.path.splitext(file_path)
    file_extension = file_extension.replace("'))", "")
    print("File Extension", file_extension)
    if file_extension == ".xes":
        log = import_xes(file_path)
        return log
    elif file_extension == ".csv":
        log = import_csv(file_path)
        return log
    else:
        print("Unsupported extension. Supported file extensions are .xes and .csv ONLY")
        exit()


# This function displays the characteristics of the Petrinet on the console
def petrinet_characteristics(net):
    print("\nCharacteristics of PetriNet\n")
    print("Count of Places: ", len(net.places))
    print("Count of Arcs: ", len(net.arcs))
    print("Count of Transitions: ", len(net._PetriNet__transitions))


# This function displays the event information
def event_info(log):
    # deleting previous file content
    f = open('event_info.txt', 'w')
    f.close()

    for case_index, case in enumerate(log):
        with open("event_info.txt", "a+") as text_file:
            print(f"\n case index: %d  case id: %s" % (case_index, case.attributes["concept:name"]), file=text_file)
        for event_index, event in enumerate(case):
            with open("event_info.txt", "a+") as text_file:
                print(f"event index: %d  event activity: %s" % (event_index, event["concept:name"]),
                      file=text_file)
    print("Event log generated. Please check your input folder for the text file")


# This function generates the information regarding the cycles in the event log
def cycle_info(net):
    # deleting previous file content
    f = open('cycles.txt', 'w')
    f.close()

    cycles = utils.get_cycles_petri_net_places(net)
    with open("cycles.txt", "a+") as text_file:
        print(f"\n cycles: %s" % cycles, file=text_file)
    print("Cycles printed in cycles.txt file. Please check your input folder for the text file")


# This function prints the evaluation metrics for the generated process model on the console
def eval_metrics(event_log, net, initial_marking, final_marking):
    fitness_tokenbasedreplay = replay_fitness_factory.apply(event_log, net, initial_marking, final_marking)
    # Perform conformance checking on the original log
    token_replay_result = token_based_replay_factory.apply(event_log, net, initial_marking, final_marking)
    print("The trace fitness for the process model is: {0:.4f}".format((token_replay_result[0]['trace_fitness'])))
    # Calculate model fitness
    print("The average fitness for the process model is: {0:.4f}".format((fitness_tokenbasedreplay['averageFitness'])))
    # Calculate process model evaluation metrics
    var_evaluation = evaluation_factory.apply(event_log, net, initial_marking, final_marking)
    # Print evaluation metrics
    print("Process model evaluation metrics extracted as follows:")
    print("Deviation point for first trace:", token_replay_result[0]['transitions_with_problems'])
    print("Percentage of traces with perfect fit: {0:.2f}%".format(fitness_tokenbasedreplay['percFitTraces']))
    print("Precision: {0:.4f}".format(var_evaluation['precision']))
    print("Simplicity: {0:.4f}".format(var_evaluation['simplicity']))
    print("Generalization: {0:.4f}".format(var_evaluation['generalization']))
    print("Average Fitness: {0:.4f}".format((fitness_tokenbasedreplay['averageFitness'])))


# This function gets the average time interlapsed between case starts
def get_case_dispersion_avg(log, parameters=None):
    """
    Gets the average time interlapsed between case ends
    Parameters
    --------------
    log
        Trace log
    parameters
        Parameters of the algorithm, including:
            PARAMETER_CONSTANT_TIMESTAMP_KEY -> attribute of the log to be used as timestamp
    Returns
    --------------
    case_arrival_avg
        Average time interlapsed between case starts
    """
    if parameters is None:
        parameters = {}
    business_hours = parameters["business_hours"] if "business_hours" in parameters else False
    worktiming = parameters["worktiming"] if "worktiming" in parameters else [7, 17]
    weekends = parameters["weekends"] if "weekends" in parameters else [6, 7]

    timestamp_key = parameters[
        PARAMETER_CONSTANT_TIMESTAMP_KEY] if PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters else DEFAULT_TIMESTAMP_KEY

    case_end_time = [trace[-1][timestamp_key] for trace in log if trace and timestamp_key in trace[0]]
    case_end_time = sorted(case_end_time)

    case_diff_end_time = []
    for i in range(len(case_end_time)-1):
        if business_hours:
            bh = BusinessHours(case_end_time[i].replace(tzinfo=None), case_end_time[i+1].replace(tzinfo=None), worktiming=worktiming,
                               weekends=weekends)
            case_diff_end_time.append(bh.getseconds())
        else:
            case_diff_end_time.append((case_end_time[i+1]-case_end_time[i]).total_seconds())

    if case_diff_end_time:
        return statistics.median(case_diff_end_time)

    return 0.0


# This function gets the average time interlapsed between the case ends
def get_case_arrival_avg(log, parameters=None):
    """
    Gets the average time interlapsed between case starts
    Parameters
    --------------
    log
        Trace log
    parameters
        Parameters of the algorithm, including:
            PARAMETER_CONSTANT_TIMESTAMP_KEY -> attribute of the log to be used as timestamp
    Returns
    --------------
    case_arrival_avg
        Average time interlapsed between case starts
    """
    if parameters is None:
        parameters = {}
    business_hours = parameters["business_hours"] if "business_hours" in parameters else False
    worktiming = parameters["worktiming"] if "worktiming" in parameters else [7, 17]
    weekends = parameters["weekends"] if "weekends" in parameters else [6, 7]

    timestamp_key = parameters[
        PARAMETER_CONSTANT_TIMESTAMP_KEY] if PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters else DEFAULT_TIMESTAMP_KEY

    case_start_time = [trace[0][timestamp_key] for trace in log if trace and timestamp_key in trace[0]]
    case_start_time = sorted(case_start_time)

    case_diff_start_time = []
    for i in range(len(case_start_time)-1):
        if business_hours:
            bh = BusinessHours(case_start_time[i].replace(tzinfo=None), case_start_time[i+1].replace(tzinfo=None), worktiming=worktiming,
                               weekends=weekends)
            case_diff_start_time.append(bh.getseconds())
        else:
            case_diff_start_time.append((case_start_time[i+1]-case_start_time[i]).total_seconds())

    if case_diff_start_time:
        return statistics.median(case_diff_start_time)

    return 0.0


# This function extracts the features from the event log
def feature_extraction(log):
    print("Feature extraction begins...")
    # Extract service time of all activities
    st = datetime.fromtimestamp(100000000)  # unixformat
    et = datetime.fromtimestamp(200000000)
    bh_object = BusinessHours(st, et)
    worked_time = bh_object.getseconds()
    print('Total service time for all the activities is {} seconds'.format(worked_time))
    print('Average time inter lapsed between case starts is {} seconds'.format(get_case_arrival_avg(log)))
    print('Average time inter lapsed between case ends is {} seconds'.format(get_case_dispersion_avg(log)))
    print("End of feature extraction")


# This function generates information with respect to cases in the event log
def case_info(log):
    cases = case_statistics.get_cases_description(log, parameters={"sort_by_index": 1, "max_ret_cases": 5})
    print("Cases: ", cases)
    duration = case_statistics.get_median_caseduration(log)
    print("Duration: ", duration)
    activities = log_attributes_filter.get_attribute_values(log, "concept:name")
    print("Activities: ", activities)
    # visualize case duration graph
    x_cases, y_cases = case_statistics.get_kde_caseduration(log)
    graph_cases = graphs_factory.apply(x_cases, y_cases, variant="cases", parameters={"format": "png"})
    graphs_factory.view(graph_cases)

    # visualize events over time graph
    x_dates, y_dates = attributes_filter.get_kde_date_attribute(log)
    graph_dates = graphs_factory.apply(x_dates, y_dates, variant="dates", parameters={"format": "png"})
    graphs_factory.view(graph_dates)
    pass


# This function is responsible for mining the process model and in turn calling all other functions.
def discover_process_model():
    log = verify_extension_and_import()
    print("Beginning Petri net mining. Please wait...")
    net, initial_marking, final_marking = inductive_miner.apply(log)
    print("Petri Net creation successful")
    feature_extraction(log)
    relevant_info_generator(net, initial_marking, final_marking, log)


# This function generates a png file that displays the petri net
def visualize_petrinet(net, initial_marking, final_marking):
    print("Visualizing Petri Net...")
    parameters = {"format": "png"}
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking, parameters=parameters)
    pn_vis_factory.save(gviz, "process_model_petrinet.png")
    print("Visualization complete and the png file process_model_petrinet.png is saved")


# This function generates a png file that displays the petri net enriched with performance information
def visualize_petrinet_performance_info(net, initial_marking, final_marking, log):
    print("Visualizing Petri Net with performance information...")
    parameters = {"format": "png"}
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking, log=log, variant='performance',
                                parameters=parameters)
    pn_vis_factory.view(gviz)
    pn_vis_factory.save(gviz, "petrinet_performance_information.png")
    print("Visualization complete and the png file petrinet_performance_information.png is saved")


# This function generates a file called resources.txt that enlists the resouces involved with each case
def resource_info(log):
    # deleting previous file content
    f = open('resources.txt', 'w')
    f.close()

    for case_index, case in enumerate(log):
        with open("resources.txt", "a+") as text_file:
            print(f"\n case index: %d  case id: %s" % (case_index, case.attributes["concept:name"]), file=text_file)
        for event_index, event in enumerate(case):
            resources = [event["org:resource"]]
            with open("resources.txt", "a+") as text_file:
                print(f"event index: %d  event activity: %s" % (event_index, event["concept:name"]), file=text_file)
                print(f"Resource used for this activity is %s".format(resources), file=text_file)
    print("Resource information is now available in resources.txt file")


# A function to display relevant information to the user
def relevant_info_generator(net, initial_marking, final_marking, log):
    user_input = input("Do you want to generate relevant data? Y/N ")
    if user_input.lower() == "y":
        user_input = input("In case of multiple selection, please separate your choices with a whitespace:\n"
                           "Please enter 1 for Petrinet Characteristics\n"
                           "2 for information with respect to the events in the log\n"
                           "3 for Visualizing Petri Net\n"
                           "4 for Visualizing Petri net with performance information\n"
                           "5 for information regarding the cycles\n"
                           "6 for displaying the evaluation metrics for the Petrinet\n"
                           "7 for displaying the various cases in the log and\n"
                           "8 for displaying Resource information\n"
                           "Please check the folder containing simulation.py file for the generated files ")
        if "1" in user_input:
            petrinet_characteristics(net)
        if "2" in user_input:
            event_info(log)
        if "3" in user_input:
            visualize_petrinet(net, initial_marking, final_marking)
        if "4" in user_input:
            visualize_petrinet_performance_info(net, initial_marking, final_marking, log)
        if "5" in user_input:
            cycle_info(net)
        if "6" in user_input:
            eval_metrics(log,net, initial_marking, final_marking)
        if "7" in user_input:
            case_info(log)
        if "8" in user_input:
            resource_info(log)


discover_process_model()
