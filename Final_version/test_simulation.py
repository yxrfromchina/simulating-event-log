# -*- coding: utf-8 -*-
import unittest
from simulation import *


class TestSimulation(unittest.TestCase):
    
    
    
    def test_read_input_file_path(self):
        self.assertIsInstance(read_input_file_path(), str)
        
    
    
    def test_import_xes(self):
        
        self.assertEqual(6, len(import_xes(read_input_file_path()))) 
    
    
    def test_get_case_dispersion_avg(self):
        self.assertNotEqual(get_case_dispersion_avg(import_xes(read_input_file_path())),0)
        
        
        
    def test_get_case_arrival_avg(self):
        self.assertNotEqual(get_case_arrival_avg(import_xes(read_input_file_path())),0)
        
        
        
    def test_discover_process_model(self):
        net,_,_ = discover_process_model()
        places = net.places
        self.assertEqual(12, len(places))
        

    
    
        
        
       
       
                     

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(TestSimulation("test_import_xes"))
    suite.addTest(TestSimulation("test_discover_process_model"))
    suite.addTest(TestSimulation("test_read_input_file_path"))
    suite.addTest(TestSimulation("test_get_case_dispersion_avg"))
    suite.addTest(TestSimulation("test_get_case_arrival_avg"))
    runner =unittest.TextTestRunner()
    runner.run(suite)
   

