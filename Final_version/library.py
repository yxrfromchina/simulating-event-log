from datetime import datetime
import statistics
from pm4py.util import constants
from pm4py.evaluation import factory as evaluation_factory
from pm4py.algo.conformance.tokenreplay import factory as token_based_replay_factory
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.evaluation.replay_fitness import factory as replay_fitness_factory
from pm4py.util.business_hours import BusinessHours
from pm4py.objects.log.util.xes import DEFAULT_TIMESTAMP_KEY
from pm4py.util.constants import PARAMETER_CONSTANT_TIMESTAMP_KEY
from pm4py.statistics.traces.log import case_statistics
from pm4py.algo.filtering.log.attributes import attributes_filter as log_attributes_filter
from pm4py.visualization.graphs import factory as graphs_factory
from pm4py.objects.petri import utils
from pm4py.simulation.montecarlo.utils import replay


def visualize_petrinet(net, initial_marking, final_marking):
    """
                This function generates a png file that displays the petri net
                Parameters
                --------------
                net
                    The petri net generated from the event logs
                initial_marking
                    The initial marking of the petri net generated from the event logs
                final_marking
                    The final marking of the petri net generated from the event logs

                """
    print("Visualizing Petri Net...")
    parameters = {"format": "png", constants.PARAMETER_CONSTANT_CASEID_KEY: "concept:name",
                  constants.PARAMETER_CONSTANT_ACTIVITY_KEY: "activity",
                  constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: "time:timestamp"}
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking, parameters=parameters)
    pn_vis_factory.save(gviz, "process_model_petrinet.png")
    print("Visualization complete and the png file process_model_petrinet.png is saved")


def visualize_petrinet_performance_info(net, initial_marking, final_marking, log):
    """
                This function generates a png file that displays the petri net enriched with performance information
                Parameters
                --------------
                net
                    The petri net generated from the event logs
                initial_marking
                    The initial marking of the petri net generated from the event logs
                final_marking
                    The final marking of the petri net generated from the event logs
                log
                    The input events in the form of a log

                """
    print("Visualizing Petri Net with performance information...")
    gviz = pn_vis_factory.apply(net, initial_marking, final_marking, log=log, variant='performance')
    pn_vis_factory.view(gviz)
    pn_vis_factory.save(gviz, "petrinet_performance_information.png")
    print("Visualization complete and the png file petrinet_performance_information.png is saved")


def resource_info(log):
    """
                This function generates a file called resources.txt that enlists the resources involved with each case
                Parameters
                --------------
                log
                    The input event logs in the form of a log

                """

    # deleting previous file content
    f = open('resources.txt', 'w')
    f.close()

    for case_index, case in enumerate(log):
        with open("resources.txt", "a+") as text_file:
            print(f"\n case index: %d  case id: %s" % (case_index, case.attributes["concept:name"]), file=text_file)
        for event_index, event in enumerate(case):
            if "org:resource" not in event:
                print("No resource information available")
                exit()
            resources = [event["org:resource"]]
            with open("resources.txt", "a+") as text_file:
                print(f"event index: %d  event activity: %s" % (event_index, event["concept:name"]), file=text_file)
                print(f"Resource used for this activity is %s" % resources, file=text_file)
    print("Resource information is now available in resources.txt file")


def petrinet_characteristics(net):
    """
                This function displays the characteristics of the Petrinet on the console
                Parameters
                --------------
               net
                  The petri net generated from the input event log

                """
    print("\nCharacteristics of PetriNet\n")
    print("Count of Places: ", len(net.places))
    print("Count of Arcs: ", len(net.arcs))
    print("Count of Transitions: ", len(net._PetriNet__transitions))


def event_info(log):
    """
                This function displays the event information
                Parameters
                --------------
                log
                    The input event logs in the form of a log

                """
    # deleting previous file content
    f = open('event_info.txt', 'w')
    f.close()

    for case_index, case in enumerate(log):
        with open("event_info.txt", "a+") as text_file:
            print(f"\n case index: %d  case id: %s" % (case_index, case.attributes["concept:name"]), file=text_file)
        for event_index, event in enumerate(case):
            with open("event_info.txt", "a+") as text_file:
                print(f"event index: %d  event activity: %s" % (event_index, event["concept:name"]),
                      file=text_file)
    print("Event log info generated. Please check your input folder for the text file")


def cycle_info(net):
    """
                # This function generates the information regarding the cycles in the event log
                Parameters
                --------------
                net
                  The petri net generated from the input event log

                """
    # deleting previous file content
    f = open('cycles.txt', 'w')
    f.close()

    cycles = utils.get_cycles_petri_net_places(net)
    with open("cycles.txt", "a+") as text_file:
        print(f"\n cycles: %s" % cycles, file=text_file)
    print("Cycles printed in cycles.txt file. Please check your input folder for the text file")


def eval_metrics(log, net, initial_marking, final_marking):
    """
                This function prints the evaluation metrics for the generated process model on the console
                Parameters
                --------------
                net
                    The petri net generated from the event logs
                initial_marking
                    The initial marking of the petri net generated from the event logs
                final_marking
                    The final marking of the petri net generated from the event logs
                log
                    The input events in the form of a log

                """
    fitness_tokenbasedreplay = replay_fitness_factory.apply(log, net, initial_marking, final_marking)
    # Perform conformance checking on the original log
    token_replay_result = token_based_replay_factory.apply(log, net, initial_marking, final_marking)
    print("The trace fitness for the process model is: {0:.4f}".format((token_replay_result[0]['trace_fitness'])))
    # Calculate model fitness
    print("The average fitness for the process model is: {0:.4f}".format((fitness_tokenbasedreplay['averageFitness'])))
    # Calculate process model evaluation metrics
    var_evaluation = evaluation_factory.apply(log, net, initial_marking, final_marking)
    # Print evaluation metrics
    print("Process model evaluation metrics extracted as follows:")
    print("Deviation point for first trace:", token_replay_result[0]['transitions_with_problems'])
    print("Percentage of traces with perfect fit: {0:.2f}%".format(fitness_tokenbasedreplay['percFitTraces']))
    print("Precision: {0:.4f}".format(var_evaluation['precision']))
    print("Simplicity: {0:.4f}".format(var_evaluation['simplicity']))
    print("Generalization: {0:.4f}".format(var_evaluation['generalization']))
    print("Average Fitness: {0:.4f}".format((fitness_tokenbasedreplay['averageFitness'])))


def get_case_dispersion_avg(log, parameters=None):
    """
    Gets the average time interlapsed between case ends
    Parameters
    --------------
    log
        Trace log
    parameters
        Parameters of the algorithm, including:
            PARAMETER_CONSTANT_TIMESTAMP_KEY -> attribute of the log to be used as timestamp
    Returns
    --------------
    case_arrival_avg
        Average time interlapsed between case starts
    """
    if parameters is None:
        parameters = {}
    business_hours = parameters["business_hours"] if "business_hours" in parameters else False
    worktiming = parameters["worktiming"] if "worktiming" in parameters else [7, 17]
    weekends = parameters["weekends"] if "weekends" in parameters else [6, 7]

    timestamp_key = parameters[
        PARAMETER_CONSTANT_TIMESTAMP_KEY] if PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters else DEFAULT_TIMESTAMP_KEY

    case_end_time = [trace[-1][timestamp_key] for trace in log if trace and timestamp_key in trace[0]]
    case_end_time = sorted(case_end_time)

    case_diff_end_time = []
    for i in range(len(case_end_time) - 1):
        if business_hours:
            bh = BusinessHours(case_end_time[i].replace(tzinfo=None), case_end_time[i + 1].replace(tzinfo=None),
                               worktiming=worktiming,
                               weekends=weekends)
            case_diff_end_time.append(bh.getseconds())
        else:
            case_diff_end_time.append((case_end_time[i + 1] - case_end_time[i]).total_seconds())

    if case_diff_end_time:
        return statistics.median(case_diff_end_time)

    return 0.0


def get_case_arrival_avg(log, parameters=None):
    """
    Gets the average time interlapsed between case starts
    Parameters
    --------------
    log
        Trace log
    parameters
        Parameters of the algorithm, including:
            PARAMETER_CONSTANT_TIMESTAMP_KEY -> attribute of the log to be used as timestamp
    Returns
    --------------
    case_arrival_avg
        Average time interlapsed between case starts
    """
    if parameters is None:
        parameters = {}
    business_hours = parameters["business_hours"] if "business_hours" in parameters else False
    worktiming = parameters["worktiming"] if "worktiming" in parameters else [7, 17]
    weekends = parameters["weekends"] if "weekends" in parameters else [6, 7]

    timestamp_key = parameters[
        PARAMETER_CONSTANT_TIMESTAMP_KEY] if PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters else DEFAULT_TIMESTAMP_KEY

    case_start_time = [trace[0][timestamp_key] for trace in log if trace and timestamp_key in trace[0]]
    case_start_time = sorted(case_start_time)

    case_diff_start_time = []
    for i in range(len(case_start_time) - 1):
        if business_hours:
            bh = BusinessHours(case_start_time[i].replace(tzinfo=None), case_start_time[i + 1].replace(tzinfo=None),
                               worktiming=worktiming,
                               weekends=weekends)
            case_diff_start_time.append(bh.getseconds())
        else:
            case_diff_start_time.append((case_start_time[i + 1] - case_start_time[i]).total_seconds())

    if case_diff_start_time:
        return statistics.median(case_diff_start_time)

    return 0.0


def feature_extraction(log):
    """
                This function extracts the features from the event log
                Parameters
                --------------
                log
                    The input events in the form of a log

                """
    print("Feature extraction begins...")
    # Extract service time of all activities
    st = datetime.fromtimestamp(100000000)  # unixformat
    et = datetime.fromtimestamp(200000000)
    bh_object = BusinessHours(st, et)
    worked_time = bh_object.getseconds()
    print('Total service time for all the activities is {} seconds'.format(worked_time))
    print('Average time inter lapsed between case starts is {} seconds'.format(get_case_arrival_avg(log)))
    print('Average time inter lapsed between case ends is {} seconds'.format(get_case_dispersion_avg(log)))
    print("End of feature extraction")


def case_info(log):
    """
                This function generates information with respect to cases in the event log
                Parameters
                --------------
                log
                    The input events in the form of a log

                """
    cases = case_statistics.get_cases_description(log, parameters={"sort_by_index": 1, "max_ret_cases": 5})
    print("Cases: ", cases)
    duration = case_statistics.get_median_caseduration(log)
    print("Duration: ", duration)
    activities = log_attributes_filter.get_attribute_values(log, "concept:name")
    print("Activities: ", activities)
    # visualize case duration graph
    x_cases, y_cases = case_statistics.get_kde_caseduration(log)
    graph_cases = graphs_factory.apply(x_cases, y_cases, variant="cases", parameters={"format": "png"})
    graphs_factory.view(graph_cases)


def case_stat(log, net, initial_marking, final_marking):
    """
                This function calculate the case_duration for each activity based on the case start time and case end time
                Parameters
                --------------
                    :param log:  The input event in the form of a log
                    :param final_marking: The final marking of activities in the process model
                    :param initial_marking: The initial marking of activities in the process model
                    :param net: The petrinet of the process model

                """

    # deleting previous file content
    f = open('case_stat.txt', 'w')
    f.close()

    cases = case_statistics.get_cases_description(log, parameters={"sort_by_index": 1})
    with open("case_stat.txt", "a+") as text_file:
        print(f"\n case statistics: %s" % cases, file=text_file)
    print("case statistics printed in case_stat.txt file. Please check your input folder for the text file")

    f = open('case_stochastic.txt', 'w')
    f.close()
    stochastic_info = replay.get_map_from_log_and_net(log, net, initial_marking, final_marking, force_distribution=None)
    with open("case_stochastic.txt", "a+") as text_file:
        print(f"\n stochastic information: %s" % stochastic_info, file=text_file)
    print(
        "stochastic information printed in case_stochastic.txt file. Please check your input folder for the text file")
